package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import states.GameStateManager;
import states.MenuState;
import states.PlayState;

public class Bomberman extends ApplicationAdapter {

	public static final int WIDTH=800;
	public static final int HEIGHT=700;
	public static final String TITLE="Bomberman";

	SpriteBatch spriteBatch;
	GameStateManager gameStateManager;

	@Override
	public void create () {
		spriteBatch = new SpriteBatch();
		gameStateManager = new GameStateManager();
		Gdx.gl.glClearColor(1, 1,1,1);
		gameStateManager.add(new MenuState(gameStateManager));
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		spriteBatch.begin();

		if(!gameStateManager.empty())
		{
			gameStateManager.update(Gdx.graphics.getDeltaTime());
			gameStateManager.render(spriteBatch);
		}

		spriteBatch.end();
	}
	
	@Override
	public void dispose () {
		spriteBatch.dispose();
		gameStateManager.clear();
	}
}
