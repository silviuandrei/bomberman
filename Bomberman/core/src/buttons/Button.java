package buttons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Button
{
    private Texture backgroundButtonTexture;

    private BitmapFont font;

    private Vector2 position;

    private String text;

    private GlyphLayout glyphLayout;

    private Color color;

    public Button(Texture texture, String text, BitmapFont font, Vector2 position, Color color)
    {
        this.backgroundButtonTexture = texture;

        this.font = font;

        this.font.setColor(color);

        this.color=color;

        this.position=position;

        this.text = text;

        glyphLayout = new GlyphLayout(font,text);
    }

    public float getX()
    {
        return this.position.x;
    }

    public float getY()
    {
        return this.position.y;
    }

    public void render(SpriteBatch spriteBatch)
    {
        spriteBatch.draw(backgroundButtonTexture,position.x,position.y,backgroundButtonTexture.getWidth(),backgroundButtonTexture.getHeight());

        font.draw(spriteBatch,text,position.x+backgroundButtonTexture.getWidth()/2-glyphLayout.width/2,position.y+backgroundButtonTexture.getHeight()/2+glyphLayout.height/2);
    }

    public void update(float dt)
    {
        if(Gdx.input.getX()>=position.x &&
        Gdx.input.getX()<=position.x+backgroundButtonTexture.getWidth() &&
        Gdx.graphics.getHeight()-Gdx.input.getY()<=position.y + backgroundButtonTexture.getHeight() &&
        Gdx.graphics.getHeight()-Gdx.input.getY()>=position.y)
            font.setColor(Color.YELLOW);

        else font.setColor(color);
    }

    public boolean isPressed()
    {
        return font.getColor().equals(Color.YELLOW) && Gdx.input.isButtonJustPressed(Input.Buttons.LEFT);
    }

    public void dispose()
    {
        this.backgroundButtonTexture.dispose();
        this.font.dispose();
    }
}
