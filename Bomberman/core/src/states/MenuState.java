package states;

import buttons.Button;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Bomberman;
import sprites.SpaceBackground;

import java.util.HashMap;
import java.util.Map;

public class MenuState extends State {

    private SpaceBackground background;

    private HashMap<String,Button> buttons;

    private Texture titleTexture;

    private Music menuMusic;


    public MenuState(GameStateManager gsm) {
        super(gsm);

        camera.setToOrtho(false,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

        background = new SpaceBackground();

        buttons = new HashMap<String,Button>();

        buttons.put("start",new Button(new Texture("button.png"),"Start Game",new BitmapFont(Gdx.files.internal("comic_sans.fnt")),new Vector2(camera.position.x-100,camera.position.y),Color.BLUE));

        buttons.put("option",new Button(new Texture("button.png"),"Option",new BitmapFont(Gdx.files.internal("comic_sans.fnt")),new Vector2(buttons.get("start").getX(),buttons.get("start").getY()-100),Color.BLUE));

        buttons.put("exit",new Button(new Texture("button.png"),"Exit",new BitmapFont(Gdx.files.internal("comic_sans.fnt")),new Vector2(buttons.get("option").getX(),buttons.get("option").getY()-100),Color.BLUE));

        titleTexture = new Texture("IconBomberman3.png");

        menuMusic =Gdx.audio.newMusic(Gdx.files.internal("menuMusic.mp3"));

        if(menuMusic!=null)
        {
            menuMusic.play();

            menuMusic.setLooping(true);
        }
    }

    @Override
    protected void render(SpriteBatch spriteBatch) {
        background.render(spriteBatch);

        for(Button button:buttons.values())
            button.render(spriteBatch);

        spriteBatch.draw(titleTexture,camera.position.x-titleTexture.getWidth()/4,camera.position.y+titleTexture.getHeight()/4+50,titleTexture.getWidth()/2,titleTexture.getHeight()/2);
    }

    @Override
    protected void update(float dt) {
        handleInput();

        background.update(dt);

        for(Button button:buttons.values())
            button.update(dt);
    }

    @Override
    protected void dispose() {

        menuMusic.stop();

        menuMusic.dispose();

        titleTexture.dispose();

        background.dispose();

        for(Button button:buttons.values())
            button.dispose();
    }

    @Override
    protected void handleInput() {
        if(buttons.get("exit").isPressed())
            Gdx.app.exit();

        else if(buttons.get("start").isPressed())
            gsm.set(new PlayState(gsm));
    }

}
