package states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

public class GameStateManager
{
    private Stack<State> states;

    public GameStateManager()
    {
        states = new Stack<State>();
    }

    public void render(SpriteBatch spriteBatch)
    {
        if(!states.empty())
        {
            states.peek().render(spriteBatch);
        }
    }

    public void set(State otherState)
    {
        if(!states.empty())
        {
            pop();
            states.add(otherState);
        }
    }

    public void add(State otherState)
    {
        states.add(otherState);
    }

    public void pop()
    {
        if(!states.empty())
        states.pop().dispose();
    }

    public void clear()
    {
        while(!states.empty())
            pop();
    }

    public void update(float dt)
    {
        if(!states.empty())
        {
            states.peek().update(dt);
        }
    }

    public boolean empty()
    {
        return states.empty();
    }
}
