package states;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PlayState extends State
{
    Texture image;

    public  PlayState(GameStateManager gameStateManager)
    {
        super(gameStateManager);
        image = new Texture("badlogic.jpg");

    }




    @Override
    protected void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(image, 0, 0);
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void dispose() {
        image.dispose();
    }

    @Override
    public void handleInput() {

    }
}
