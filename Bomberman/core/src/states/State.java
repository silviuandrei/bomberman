package states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public abstract class State
{
    OrthographicCamera camera;
    Vector3 mouse;
    GameStateManager gsm;

    protected State(GameStateManager gsm)
    {
        this.gsm=gsm;

        mouse = new Vector3();

        camera = new OrthographicCamera();
    }

   protected abstract void render(SpriteBatch spriteBatch);
   protected abstract void update(float dt);
   protected abstract void dispose();
   protected abstract void handleInput();
}
