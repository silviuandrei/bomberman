package sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Bomberman;

import java.util.ArrayList;

public class SpaceBackground
{
    private Texture backgroundTexture=null;

    private ArrayList<Sprite> backgroundSprites=null;

    private OrthographicCamera camera=null;

    public SpaceBackground()
    {
        backgroundTexture = new Texture("background.png");

        backgroundSprites = new ArrayList<Sprite>();

        backgroundSprites.add(new Sprite(backgroundTexture,0,0,backgroundTexture.getWidth(),backgroundTexture.getHeight()));

        backgroundSprites.get(0).setPosition(0, Bomberman.HEIGHT-backgroundTexture.getHeight());

        backgroundSprites.add(new Sprite(backgroundTexture,0,0,backgroundTexture.getWidth(),backgroundTexture.getHeight()));

        backgroundSprites.get(1).setPosition(0,backgroundSprites.get(0).getY()-backgroundSprites.get(0).getHeight());

        backgroundSprites.add(new Sprite(backgroundTexture,0,0,backgroundTexture.getWidth(),backgroundTexture.getHeight()));

        backgroundSprites.get(2).setPosition(0,backgroundSprites.get(1).getY()-backgroundSprites.get(1).getHeight());
    }

    public void render(SpriteBatch spriteBatch)
    {
        for(Sprite sprite:backgroundSprites)
            sprite.draw(spriteBatch);
    }

    public void update(float dt)
    {

        for(Sprite sprite:backgroundSprites)
            sprite.translate(0,3);

        if(backgroundSprites.get(0).getY()> Gdx.graphics.getHeight())
           backgroundSprites.get(0).setPosition(0,backgroundSprites.get(2).getY()-backgroundSprites.get(2).getHeight());

        if(backgroundSprites.get(1).getY()> Gdx.graphics.getHeight())
        backgroundSprites.get(1).setPosition(0,backgroundSprites.get(0).getY()-backgroundSprites.get(0).getHeight());

        if(backgroundSprites.get(2).getY()> Gdx.graphics.getHeight())
            backgroundSprites.get(2).setPosition(0,backgroundSprites.get(1).getY()-backgroundSprites.get(1).getHeight());
    }

    public void dispose()
    {
        this.backgroundTexture.dispose();
    }
}
